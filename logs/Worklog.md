# Worklog

| Week  |What|
|-------|----|
| 21.02 | Requirements and bootstrap |

## Week of 21.02

- Organisational meetings.
- Research Keycloak as per advisor requirements.
- Research Keycloak alternatives -> ORY Kratos.

## Week of 28.02

- Meeting HEIG-Bity
- Read on ORY Hydra to dig into what exists.

## Week of 07.03

- Weekly meeting.
- Read on OAUTH 2 flows.

- Lectures
  - Hydra.
  - IdP: keycloak, kratos, authelia
    - keycloak au-delà de la simple authentification
      - ! mais probablement configurable
      - il est normal que l'IdP stocke l'identifiant
      - Pas besoin d'utiliser toutes les fonctionnalités gestion de profil.

    - kratos beaucoup plus API-first
      - délègue l'interface GUI
      - ! attention aux bugs
        - patchabilité
        - audit sécurité

  - Gestion des facteurs d'authentification ? standard?

- analyse existant
  - hydra est l'authorisation server

## Week 0f 21.03

Objectives:

- Schéma intégration avec les composants existants.
- Établir liste de critères de comparaison.

## Week of 13.04

Étonnement:

- Méthodologie de la recherche.

## Week of 04.05

Étonnement:

- Identification stories: FIDO2 + TOTP *because* ???.

- Login application -> Separate diagram.

- Consent application -> Separate diagram.

- Present identification stories.

- Recovery in case of lost authentication factor.

- We do not model the case when the identification process succeeded for an
  unauthorized party.

- Send an email after authentication factors changed.

- Let the user decide what identification flows remain available upon changes
  in authentication factors.

## Week of 07.05

## Week of 21.06

Should address:

- Differences between the design and the prototype implementation.

=> The prototype UI should be reworked.

- Argument.
- Sketch UI of the proposed login flows.

Questions:

- Some data packet have been simplified (e.g. RP, CredentialID, ...) as
  superfluous, is this acceptable.
- Some vulnerabilities (e.g. authenticator unavailable in UC7, UC8) were simply
  not taken into account because they seem extremely unlikely or "évident".


Rework:
- Add degenerate cases to DFD. (UC5 => Disclosure).

## Week of 04.07

Questions :

J'ai toujours de la peine à voir un processus "complet" afin de faire et
maintenir une évaluation de menaces :

- L'on peut faire une analyse de menaces du système (e.g. STRIDE).
- L'on peut faire une recherche exhaustive de vulnérabilités dans le code.

Mais j'ai de la peine à voir

- Ce qui viendrait guider un processus de "sécurité continue".
- Le lien entre l'analyse du système et la recherche de vulnérabilités.

TODO :

- Envoyer agenda de la présentation prototype.

- Montrer une démo du prototype (max 10 threats détaillés les plus importants).
  - Expliquer les flux de données.
  - Expliquer les vulnerabilités trouvées.
  - Comment elles sont traitées.

- Suite à mon congé : Viser la livraison du prototype (florian peut review).
  Faire un pas en arrière afin d'adresser ce qui est sorti de l'analyse que je
  vous ai présenté.

- Modélisations qui manquent concernant la gestion du token.

- Ajouter une section prototype dans le rapport.
  - Comment ce qui est implémenté répond au design (et différences).
  - Remarques discutées lors de la présentation du prototype.

# Week of 11.07

- I'm confused about V-UC3.1.E, since a _data flow_ cannot suffer from elevation
  of privilege. I think the threat resides in b.T and the current b.T describes
  a vulnerability.

# Week of 18.07

- Need to specify that OpenID connect is a particular case of the OAuth 2.0
  authorization flow for identity.
- I feel like the threat mitigations table should be flattened into the threats
  (add countermeasure subsection).
- The state-of-art section contains the comparison criteria, this is unrelated
  and should be moved to somewhere what it's not confusing.
- The section _System and threat model of the prototype_ was removed in favor
  of the threat analysis annex. But this annex is yet to be somewhere
  introduced.
- Add a "conclusion" section with a quick overview of the points I wanted
  to put into a single section, but ended up across the different sections.

TODO:

- Description of project delimitation.
- Diagrams of self-service token management flows.
- Diagrams of account access recovery flows.
- Conclusion
- Annex 2: Description of sweng strategies and practices at Bity.
- Add introduction to a security policy on the threat analysis.
