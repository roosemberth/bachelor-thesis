Keycloak is an identity and user management system:

- User authentication (local, ldap, SAML, OpenID Connect).
- Realms as identity namespaces.
- Session management.
- Identity clients management (applications).
- Per-role policy.
- Ships UI, hard to instrument API: You can use "themes" by adding classes to the classpath.
- Per-scope policies.

Ory kratos is an Identity and User management system:

- API-first. Strong separation of concerns.
- User authentication (local, OpenID Connect).
- Admin APIs for managing identities.
- Flexible data-models.
- 20MB static binary.
- Privileged sessions.
