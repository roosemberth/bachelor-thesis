- What is FIDO2?
- What is WebAuthn?
- What other widely implemented protocols support interacting with
  authentication factors?
- What entities FIDO2 provides?

## FIDO2

At its core, FIDO2 consists of
  - The Client to Authenticator Protocol (CTAP).
  - The W3C standard WebAuthn.

FIDO2 enables users to identify themselves with
  - cryptographic authenticators (such as biometrics or PINs)
  - external authenticators (such as FIDO keys, wearables or mobile devices)
to a trusted WebAuthn remote peer (also known as a FIDO server).

### How does FIDO work?

FIDO keys are generated and verified upon registering a FIDO authenticator:
  - The user registers with FIDO server and generates a new key pair on the
    authenticator - consisting of a private key and a public FIDO key.

  - While the private key is stored on the authenticator and is only known on
    the client side, the public key is registered in the FIDO server's key
    database.

  - Subsequent authentications are now only possible by verification with a
    private key, which must always be unlocked by a user action.

## FIDO CTAP

- Credential generation => (Public key, credential ID) bound to the
  authenticator manufacturer (attestation CA), the authenticator and the
  relaying partner (FIDO2 server), and an identity for the user in the
  relaying partner.

- Credential discovery: Per-credential policy whether credential is
  discoverable.

- Credential proving: A relying party may assert the existance of a credential
  in an authenticator.

- Credential policy may dictate whether user presence (up) or user verification
  (uv) is required to use or acknowledge the existance of a credential.

- Protocol is oriented towards providing flows to preserve the user privacy.

- Cluttered with backwards compatibility considerations.

## Webauth

- Provides scoped identities based on ???.

- Protocol is oriented towards providing zero-knowledge flows to respect
  the user privacy on both the authenticator and relaying partner.

- Non-normative privacy and security recommendations to prevent epistemic
  leakage.

- Client-side credentials (discoverable), server-side credentials (probable
  only if the relaying party provides a recognizable credential ID).

# References

<https://www.ionos.com/digitalguide/server/security/what-is-fido2/>
<https://www.w3.org/TR/webauthn/#sctn-security-considerations-rp>
