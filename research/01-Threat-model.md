# Preface

## Objective

- System-level modeling.
- Definition of a [_trusted computing base_ (TCB)][tcb] and a threat model.

## Strategy

- Model the solution in context.
  - Context (actors, actions), Processes (For given action: actors, steps),
    Use-case (For given business-case: actors, steps, messages).

- Identify it weaknesses, per arrow ^.
  - Spoofing
  - Tampering
  - Repudiation
  - Information disclosure
  - Denial of Service
  - Elevation of privileges

- Provide counter measures.

## Why am I calling?

I didn't know where to start, mais les slides ont beaucoup aidé.

# System modeling

What components the prototype is interacting with?

- Follow the data-flow: Actors, Components, Systems, Processes, Actions, ...

# Cours - Stride

- Spoofing invoker's identity.
- Tampering data on SPT(storage, in process, in transit.).
- Repudiation of an action by the owner.
- Information disclosure on SPT.
- Denial of service of process, data store or interface.
- Elevation of privileges: Bypassing authorization on data, process or interfaces.

Identify objective:

- Users place exchange orders through a web application.
- Users must be authenticated to place an order => Have user authorize webapp.

Iterate by refinement of data-flow diagrams:

- Context model (Actors):
    - A user has configured Username + WebAuthn SFA: User, Authenticator, Backend.
- Process model (Components, systems):
    - User sends login request, backend credential signature, user requests
      authenticator to sign, ...
- Data flow (Actions):
    - Send Username: LB -> Backend, Request WebAuthn signature: Backend -> LB -> User,...

Identify application threats:

- Tampering of the data-store storing the allowed flows for this user.
- Elevation of privilege: Bypassing the WebAuthn credential signature.
- Information disclosure of the WebAuthn signature.

# Questions

## My project does not perform business actions, but rather support actions

From the user point of view: Users authenticate "in order to...".

It is not the same thing to "authenticate the user and then execute an action"
than "requesting an authorization from a user, who then will be authenticated
in order to provide authorization".

Where do I limit my threats?
- An unauthorized user places an order.
- An unauthorized user grants the application a valid authorization to...
- The identification application authenticates an unauthorized user.

### Discussion

Will be limited once we have a system modeling: This question is equivalent to
"Where does my system model stop?".

NB:

One can make threat models at many tiers: Application, platform, ...
However, we're likely to limit ours to a single one.

## Link between process vuln and software vuln

System model will give me information about business flow, like interfaces and
data stores, but not about support structures (such as OS, libraries, ...).

- J'ai de la peine à faire le lien entre les vulnerabilités "haut niveau" (dans
  le processus) et des vulnerabilités logiciel (e.g. bad input sanitization,
  SQL injection).

### Discussion

- We're unlikely to get into such details.

- However, we have to discuss about how would we treat such vulnerabilities
  after being identified: This may be more complex than we think:

  Whichever software we use for our prototype, we'll probably end up modifying.
  We need to think about how upstream software will continue to evolve:
  In particular, when software diverges from our use-case, what our strategy
  for fixing the vulnerability will be.

## Constructing a TCB

TCB: Properties we can assume won't fail.
An assertion about a security property of a system can only be made on top of a
TCB.

It seems odd to build a TCB from the identified threats,
by "discarding them" (or rather silencing them by moving them to the TCB).

- Execution environment (namespaces PID, Mount, ...) are secure.
- Database connection is secure (no MITM).
- ...

- How do I model my TCB? Do I have sufficient information to do so?

### Discussion

TCB => Hypothèse de confiance.

We derive it from the system model by **selecting** components that will not be
analysed and providing a hypothesis for this.

## Viability / likeness of an attack

- At what moment do I talk about the finantial viability of an attack?

e.g. Breaking SHA1 (or SHA3 for that matter) is technically possible, with
enough computing power.

### Discussion

~~Viability of an attack~~ => Ménaces crédibles.

Once we've identified threats, keep only those we estimate to have a credible
impact and only work on those.

## Addendum

A threats are modeled against some business threats:

- Select the top business threats which are important to the missing
  (e.g. reputation, image, mission statement).
- For each identified threat, evaluate into a score (threat score):
  - The level of exposure.
  - The "size" of the vulnerability.
  - How likely the threat is. (Think threat credibility.)
- For each threat, evaluate the impact on each individual business threat and
  calculate a weighted sum (business impact).
- Rank = threat score × business impact
