---
title: Description of software engineering strategies and practices at Bity
date: 25.03.22
author: Roosembert Palacios
bibliography: refs.bib
link-citations: true
---

# Introduction?

Most software platform in our day-to-day operations are managed on-site.
This includes software critical to our customer-related operations, but also
support solutions for development and operation of such services.

## Public

From the point of view of the software platform, a **user** is any of
the following:

- Unidentified costumers.
- Identified costumers.
- Backend operators
- Client support operators.

In particular, BI officers are not part of the intended users and such
requests are manually processed by the engineering team.
However, components should allow service operators to query and extract the
necessary data with relative ease.

## User platform overview

Our solutions engineering is driven by a microservices-oriented architecture
(see @awsMicroservices) built on top of open source technologies.

In particular, this notion is also applies to authorization: Services providing
user-oriented functionality should validate an OAuth 2.0 access token though
token introspection (@rfc7662).

### Account authentication and authorization (AAA) approach

Our AAA system is designed with a modular approach.
Some components have not yet reached maturity but provide instead blanket
implementations often times missing some degree of functionality.
This gives us the flexibility of taking an agile approach focused on feature
development, which may require integration from multiple components;
while continuously providing ever-growing value to our customer operations.

Our AAA system allows for the following components:

Cardinality | Responsibility                 | Implementation(s)
---         | ---                            | ---
1           | Authorization server           | Hydra*
1           | Consent application            | Jasmine
1           | Login application              | Iris
1..         | Identity provider              | Bity account provider
0..         | Security factor provider       | (no implementations exist)
1           | Account recovery application   | (no implementation exist)

The diagram bellow presents the components requiring some form of coordination
surrounding the _login application_.

```mermaid
graph TD;
  AS(Authorization server);
  BAP(Bity account provider);
  IDP(Identity provider);

  LA(Login application);
  SFP(* Security factor provider);
  AR(* Account recovery service);

  AM(Account management application);

  AS --- LA;

  LA --- IDP;
  LA --- SFP;
  LA --- BAP;
  AM --- AS;
  AM --- BAP;
  AM --- IDP;
  AM --- SFP;
  AR --- BAP;
  AR --- IDP;
  AR --- SFP;
```

#### Authorization server

The authorization server arbitrates user authorization requests throughout our
platform. It performs the role of _authorization server_ as described in the
The OAuth 2.0 Authorization Framework (@rfc6749).

The following diagram depicts a web application execution some operation (e.g.
_place an exchange order_) on some resource (e.g. `bity_account_oauth`,
`bity_account_client_oauth`) with some scope (e.g.
`https://auth.bity.com/scopes/exchange.place`).
This process requests authorization from the resource owner (e.g. the "end user"
or the client).

The use-case depicted bellow applies for both our self-service applications
(such as <https://my.bity.com>) and 3rd party API partners with very little
differences. Such differences are explicated in the diagram bellow.

```mermaid
sequenceDiagram
  actor U as User agent (e.g. web browser)
  participant WA as Web application
  participant RS as Resource server<br/>(e.g. Exchange API)
  participant AS as Authorization server

  U ->> WA: User performs an operation<br/>requiring webapp to access<br/>resource with some scopes

  alt Client may use bity_account_client_oauth authorization
    Note right of WA: The resource owner is the client.
    alt Webapp does not have viable access token on bity_account_client_oauth with the required scopes
      Note right of WA: Retrieve access token through OAuth 2.0
      alt Webapp is private (i.e. internal to Bity)
        Note right of WA: Use authorization code, client credentials or refresh token
      else Webapp is public
        Note right of WA: Use authorization code or refresh token
      end
    end
  else Client may use bity_account_oauth authorization
    Note right of WA: The resource owner is the user.
    alt Webapp does not have viable access token on bity_oauth_account with the required scopes
      Note right of WA: Request OAuth 2.0 Authorization grant through<br/>refresh token or authorization code flow.
    end
  end

  WA ->> RS: Place order with access token
  RS ->> AS: Retrieve client information
  AS ->> RS: Client token information

  alt Client has requires scopes for requested operation
    Note right of RS: Place order
    RS ->> WA: URL to placed order
  end

  WA ->> U: Feedback about the order
```

The following diagram illustrates a web application requestion an OAuth 2.0
authorization grant through an authorization code flow.
Where the resource owner is an "end user"  interacting with a web application
(the client) through a web browser (user agent).

```mermaid
sequenceDiagram
  actor U as User agent (e.g. browser)<br/>operating on behalf of the<br/>resource owner (user)
  participant WA as Web application
  participant AS as Authorization server
  participant LA as Login application
  participant IP as Identity provider
  participant CA as Consent app

  U ->> WA: User performs an operation<br/>requiring webapp to access<br/>resource with some scopes
  alt Webapp does not have access token with the required scopes
    WA ->> U: Redirect to authorization server<br/>with webapp client_id<br/>and response_type=code
    U ->> AS: GET /auth?client_id=...&response_type=code
    alt User agent has not acceptable session (not authenticated)
      AS ->> U: Redirect to Login application with login challenge
      U ->> LA: GET /login?login_challenge=...
      LA ->> U: Login form with login challenge
      U ->> LA: POST with credentials and login challenge
      LA ->> IP: Verify user credentials
      IP ->> LA: OK
      LA ->> AS: Accept login<br/>with login_challenge
      AS ->> LA: Redirect to<br/>Authorization server
      LA ->> U: Redirect user to Authorization server
      U ->> AS: GET /redirect_url
    end
    alt Webapp requests some scope for the first time
      AS ->> U: Redirect to Consent application with consent challenge
      U ->> CA: GET /consent?consent_challenge=...
      alt client is not in implicit consent list
        CA ->> U: Consent form with consent_challenge
        U ->> CA: POST /consent/accept?consent_challenge=...
      end
      CA ->> AS: Accept consent with consent_challenge
      AS ->> CA: Redirect to Authorization server
      CA ->> U: Redirect user to Authorization server
      U ->> AS: GET /redirect_url
    end
    AS -->> AS: Verify grant
    AS ->> U: Redirect user to webapp with authorization code
    U ->> WA: GET /back
    WA ->> AS: Exchange authorization<br/>code for access token
    AS ->> WA: Issue access token<br/>(maybe refresh token)
  end
```

Note that the _Security factor provider_ application is not displayed
in this diagram since such is an implementation detail of the login application.
Mind that in this diagram the login application authenticated against the
identity provider on the user's behalf:
This is an implementation detail; the login application has control of the user
agent and may take judicious action on how to authenticate the user.

The flow for retrieving a `bity_account_client_oauth` authorization similar to
the diagram above; details about what actors may assume the role of client and
resource owner in the diagram above are left to the client implementation but
follows the allowed authorization grants flows specified in the previous
diagram as specified in @rfc6749 section 4.

The flow for requesting an OAuth 2.0 authorization grant through refresh token
in either case follows the procedure as described in @rfc6749 section 6.

#### Consent application

The consent application should receive the requested scopes and display a form
to the user containing the name of the client in a disposition the user can
understand, the list of scopes requested by the client and a description of
what they imply in a disposition the user can understand.

#### Login application

The login application receives a user with a login challenge and is responsible
of identificating the user.
Currently, the only supported authentication method is the Bity account
username/password; provided by the _Bity account identity provider_.

#### Account recovery application

This component is does not currently exist.
Such requests are received by the support team and treated by the engineering
team.

## Execution environment

- Staging and prod.
- Internal HTTPS.
- Asynchronous callbacks.
- Stateless microservices.

## Deployment strategy

- Debian packaging, internal package archives.
- Tag (release) -> CI -> packaging -> publish to staging.
- Bity-infra -> Ansible -> AWX (deploy by maintainers).
- Staging validation -> publish to prod -> AWS.

## Continuous deployment

- Publish on tag pipelines.
- All pipelines generate packages.

## Continuous integration

- Unit tests as part of the packaging process.
- Integration tests on installed packages.

## Software development

- Prefer strongly typed languages.
- OCI containers based on the target environments.
- Stay as close as possible to the programming language standards, the Debian
  policy and if exists the debian policy particular to that programming
  language.

### Feature process

- Features as issues.

### Contribution process

- MR for functionnalities.
