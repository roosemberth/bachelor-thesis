# Evaluation of possible solutions

## Comparison criteria

- Estimated integration effort into Bity's engineering strategy, as described
  in the formal requirements.
  - Estimated effort to implement Hydra's login flow.
  - Estimated effort of packaging and deployment automation.

- Strategy against security incidents.
  - Whether a model validation of the software exists.
  - Whether 3rd party audits exist.
  - Evaluations and reviews about the software by security experts.

- Existing ecosystem.
  - Relevance of community contributions.
  - Strength and feedback of user forums.
  - Strength and feedback of developer forums.
  - Key players/stakeholders.

- Estimated effort for the prototype.
  - Estimated effort to implement the selected flows for registration,
    login and token lifecycle.
  - Extensibility for more authentication factors and flows.

## Solution based on Keycloak

### A rough idea of how an implementation based on Keycloak would look

Officially supported flows for securing web clients:
<https://www.keycloak.org/docs/latest/securing_apps/index.html>.

Keycloak supports OpenID Connect and SAML.

=> OpenID Connect adapter.

Since ORY Hydra has strict requirements on the protocol in order to log
a user in, a web application functionning as adapter between ORY Hydra and
Keycloak is needed.

In conclusion, develop and test:

<!-- XXX: FIXME: Make this a mermaid diagram. -->

- ORY Hydra <-> Adapter <-> Keycloak.

The adapter would receive the user-agent from Hydra, redirect them to keycloak,
have keycloak validate them and upon receiving the user-agent back, submit the
login flow result to Hydra and finally redirect the user-agent back
to the authentication server.

Since ORY Hydra's login flow is very similar to the use of OpenID Connect in
this context, the adapter would be pretty thin.

> XXX: TODO: Sketch an implementation of each user-authentication and token
> management stories.

From [Keycloak's documentation][kc-securing-apps-oid]:

[kc-securing-apps-oid]: https://www.keycloak.org/docs/latest/securing_apps/index.html#openid-connect-2

> After a successful login, the application will receive an identity token and
> an access token. The identity token contains information about the user such
> as username, email, and other profile information. The access token is
> digitally signed by the realm and contains access information (like user
> role mappings) that the application can use to determine what resources the
> user is allowed to access on the application.

Both the identity and access tokens would be verified by the adapter.
Upon validating these tokens, the information therein would be used to
construct the login flow result and submit it to ORY Hydra.
Details can be found in the [corresponding section][keycloak-admin-oid] in the
keycloak server administration documentation.

[keycloak-admin-oid]: https://www.keycloak.org/docs/latest/server_admin/#con-oidc_server_administration_guide

> XXX: Add a diagram representing: ORY Hydra, the adapter and Keycloak.

> XXX: Add section about data persistance & ownership.

#### Alternative implementations

- Write a Keycloak extension:

  While writing an additional "SSO protocol" (as referred to in the server
  administration) for Keycloak should be possible, I could not find any
  documentation for this.
  I posted about this in the mailing list without any response:
  <https://groups.google.com/g/keycloak-user/c/U2fyncOyekg>.

- Modify Keycloak itself to implement ORY Hydra's login flow:

  This solution would require a heavy maintenance effort (since such flow is
  not supported and keycloak updates would likely have to be manually
  backported.

### A strategy for testing

- Generate container images that run each entity with a test configuration.

Development:

- docker-compose running all of the test images.
- Test script calling [Cypress](https://www.cypress.io/) for the implemented
  flows: e.g. log the user in.

Continuous integration:

- Run each test image as a CI "service".
- Run the test script.

### Packaging and deployment

Debian approach:

Packaging for Debian may prove to be a lot of work.

Fortunatelly, Bity is working in migrating towards a deployment approach based
on Docker and Kubernetes (https://kustomize.io/).
It is thus acceptable to provide a docker image and a [Kustomization][]
kubernetes resource instead of a debian package and ansible automation.

[Kustomization]: https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/

Kubernetes approach:

The official [Keycloak documentation provide kubernetes examples][keycloak-k8s]
based on the official docker image.
This moves the integration effort to kubernetes configurations instead of
packaging and maintaining debian packages and ansible playbooks.

[keycloak-k8s]: https://www.keycloak.org/getting-started/getting-started-kube

### Community

- Great community exposure: https://www.keycloak.org/community
- User forums get one or two dozen messages a week, a lot go unanswered though.
- User mailing list receives a couple threads a day and seems overall more
  reactive.

There are many community-mainained plugins highlighted on Keycloak's website,
and many more scattered over the internet.
However, there are no plugins implementing ORY Hydra's login flow.

The project appears to be very contributor friendly receiving contributions
of 37 different authors over the last month (2.05.22-2.06.22).
Source: <https://github.com/keycloak/keycloak/pulse>.

### Strategy for security incidents

Procedure for reporting recurity vulnerabilities:
<https://www.keycloak.org/security.html>.
The procedure suggests Keycloak developers encourage working with the research
author on a fix for the perceived vulnerability in a private repository before
publication.
I could not find any particular notification system for security incidents,
other than "keep your versions up-to-date".

A quick search on <https://www.researchgate.net> and Google scholar revealed a
couple results, none of which were focused on a security or threat model though
they may still prove useful.
One of this was published in the International Journal of Security and Privacy
in Pervasive Computing (IJSPPC), access to the specific article was requested.

- A Review on Identity and Access Management Server (KeyCloak).
  DOI:10.4018/IJSPPC.2020070104
- Using Keycloak for Gateway Authentication and Authorization.
  DOI:10.6084/m9.figshare.5483557.v1

### Pros-cons

Pros:

- The user identification process is ready-to-use and thus faster to
  prototype with.
- Adapter application simple enough.
- Since keycloak is the biggest component in the solution, a very large
  portion of the solution has been and continues to be tested by a large
  community.

Cons:

- A large part of the features implemented by Keycloak is unused in the
  solution and cannot be disabled, thus providing a larger attack surface.
- Personalizing the UI of Keycloak may prove challenging later.
- The additional redirections of the user-agent may seem sluggish to a user
  with a limited internet connection.

## Solution based on ORY Kratos

### A rough idea of how an implementation based on Keycloak would look

Since ORY Kratos does not provide any user interface, a web application is
required to use the Kratos API effectively.
Kratos supports different flows to integrate with server-side (i.e. through an
application server) or client-side only (e.g. ReactJS, AngularJS, ...)
applications. The Kratos documentation refers to the web application in the
server-side case as the _flow UI_.

In the server-side case:
When ORY Kratos receives the user-agent in the "login" endpoint, the user-agent
will be redirected to the flow UI (with a `flow-id` query parameter).
The flow UI then fetches the data necessary to render the HTML login or
registration forms and then sends the response back to the user-agent.
The user-agent may be instructed to directly submit the credentials to Kratos.

In the client-side case:
The client-side application sends AJAX requests (with a JSON payload) to Kratos
and uses the response to render the HTML login or registration forms.
The client-side application then submits the credentials to Kratos.

Upon succesful authentication, 

=> Server-side application

Since ORY Hydra's login flow requires as component to provide the login flow
response, such component is trusted and can thus not reside in the user-agent.

In conclusion, develop and test:

<!-- XXX: FIXME: This REALLY needs to be a mermaid diagram. -->

- User agent <-> ORY Hydra <-> Flow UI <-> Kratos.

The flow UI would receive the user-agent and render the adequate HTML forms.
The user agent would then submit credentials to Kratos, which upon succesful
authentication provides an "Ory Kratos Session Token" (henceforth session token)
either by (Cookier + HTTP redirection or AJAX response).
The session token would then be then used by the the flow UI to query the
session on Kratos and submit the login flow result to Hydra, finally redirecting
the user back to the authentication server.

From [ORY Krato's documentation][kratos-login-success]:

[kratos-login-success]: https://www.ory.sh/docs/kratos/self-service/flows/user-login#successful-login

> When the login is completed successfully, Ory Kratos responds with a HTTP 303
> Redirect to the configured redirect URL. Alongside the HTTP 303 Redirect is a
> Set-Cookie header which contains the Ory Kratos Login Session Cookie [...].

The flow UI is responsible of rendering the HTML to the user, validating the
session token and constructing the login flow result.
This would constitue a sizeable web application and require implementation and
testing of defensive security measures such as CSFR tokens to ensure the
security of the system.

> XXX: Add a diagram representing: ORY Hydra, the flow UI and Kratos.

> XXX: Add section about data persistance & ownership.

#### Alternative implementations

- Write a client-side application instead and a small session token validation
  application:

  This solution would be viable aswell, but likely to require more engineering
  work since it would require the maintenance of two different (co-dependent)
  applications.
  The advantage of this approach is that this could prevent CSFR-related
  vulnerabilities, at the expense of complexity in the client-side logic.

### A strategy for testing

- Generate container images that run each entity with a test configuration.

Development:

- docker-compose running all of the test images.
- Test script calling [Cypress](https://www.cypress.io/) for the implemented
  flows: e.g. log the user in.

Continuous integration:

- Run each test image as a CI "service".
- Run the test script.

### Packaging and deployment

Debian approach:

- Flow UI:
  Bity has many similar web applications with well-established packaging and
  deployment procedures, adding another one should be simple enough.
- ORY Kratos:
  Packaging for Debian is mentioned in the community forums with some pointers
  to how to do it, but no actual packaging exist.
  Packaging Kratos for Debian may or may not be a challenging task.

Kubernetes approach:

The official [ORY Kratos documentation provides kubernetes examples][kratos-k8s]
provides a both a helm chart and docker image.
Special care should be taken to match the upstream helm chart to a
Kustomization definition.

[kratos-k8s]: https://www.ory.sh/kratos-knative-demo/

### Community

https://community.ory.sh/

### Strategy for security incidents

### Pros-cons

## Comparison of proposed solutions

