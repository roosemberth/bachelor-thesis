---
title: Comparison criteria among user authentication systems
date: 25.03.22
author: Roosembert Palacios
bibliography: refs.bib
link-citations: true
---

## Scope

The objective of this comparison criteria is to provide a quantitative and
qualitative frame for evaluating software that in conjunction with some
integration should facilitate the implementation of one or many web applications
to use as "login application" in accordance with the existing OAuth 2 system in
place at Bity.
The login application is responsible of determining who the user based on
identification factors such as (but not limited to) username, password, TOTP,
WebAuthn and/or FIDO.

This document does not address whether the combination of any of any 3rd party
solutions and integration software into a final solution is preferable to a
complete in-house solution.
Rather, such decision should be derived from the results provided by using this
document.

The [original proposal](../publications/Proposition TB Roosembert - Bity.md)
(@originalProposal) requires the final solution to provide the following key
criteria:

- Support for multiple identification factors.
- Allows to set the user identification factors.
- Allows to replace a user identification factors.
- Allows to recover a user account after having lost their identification
  factors.

Thus the final solution constructed on the software evaluated through the
criteria provided here MUST NOT prevent any of these objectives, but SHOULD
rather constructively collaborate towards a solution providing these properties.

The final solution MUST NOT be a nuisance in the existing ecosystem of software
components at Bity, described bellow.

Finally, it is strongly preferable that any 3rd-party components in the final
solution be Open Source.

## Context

The existing OAuth 2 system in place at Bity allows for a modular
microservice-oriented architecture.

The diagram bellow presents the components requiring some form of coordination
surrounding the _login application_ (described as _identification application_
in the original project proposal).
These components are part in the implementation of the OAuth 2.0 authorization
framework (as described in @rfc6749). The diagram bellow does not take into
account user flows.

In green, the existing components, the _Login application_ is subject to be
modified or replaced.
The star (*) represents components that SHOULD be part of the final solution,
but may not be implemented on the provided deliverable.
It should be noted that the shape of the diagram provides no constraints on
the implementation architecture; for instance all uncolored components may be
provided by one or many separate applications.

```mermaid
graph TD;
  AS(Authorization server);
  BAP(Bity account provider);
  IDP(Identity provider);

  LA(Login application);
  SFP(* Additional security factor provider);
  AR(* Account recovery service);

  AM(Account management application);

  AS --- LA;

  LA --- IDP;
  LA --- SFP;
  LA --- BAP;
  AM --- AS;
  AM --- BAP;
  AM --- IDP;
  AM --- SFP;
  AR --- BAP;
  AR --- IDP;
  AR --- SFP;

  style AS fill:#0a6d01
  style BAP fill:#0a6d01
  style IDP fill:#0a6d01
  style AM fill:#0a6d01
  style LA fill:#0a6d012f
```

- The _Authorization server_ (implemented by [Ory Hydra][]) provides the OAuth
  2.0 API. In particular, it delegates the user authentication to the
  _login application_ and any other subsequent security factors.
- The _login application_ component in charge of determining who the
  user is. To do so, it may request zero or more additional security factors
  from the user. The current Login application implements a single
  identification factor: login and password. The structure of the existing
  system is such that this application can easily be replaced.
- The _account recovery service_ allows a user to replace lost identification
  factors.
- The user identity may be provided by one of many _identity provider_; for
  instance the user identity may be provided through OpenID connect and is able
  to identify and authenticate a user.

[Ory Hydra]: https://www.ory.sh/docs/hydra

In particular, the _login application_ is expected to implement
[Hydra's Login flow](https://www.ory.sh/docs/hydra/concepts/login).
In summary, it receives an end user with a login challenge, authenticates the
user, validates the login using the login challenge and finally redirects the
end user.
Mind that the 'consent application' referenced in the Hydra login flow is
part of the Bity OAuth 2 system but not represented in this diagram and is
outside the scope of this document.

### Technology review

- OAuth 2.0.
- TOTP.
- WebAuthn
- FIDO.

TODO.

## 3rd-party user identification software and necessary integration

### Motivation

A motivations to include 3rd party software in the final solution is to delegate
security-critical flows (such as user identification) to battle-tested software
as opposed to running an in-house implementation.

Such software should preferably have a small attack surface and some strategy
against security incidents. Such strategies usually drive the design and
maintenance cycle and are also present upon the discovery of a vulnerability
to the release of security patches.

The modeling and quantification of the risk of security in a software system
has been studied over the years.
@Okamura2012Nov presents a stochastic model with a vulnerability life-cycle
model using continuous-time Markov chains.
@Okamura2013Apr presents a quantitative software security model based on the
vulnerability discovery process and provides a practical example on a popular
content management system from publicly available data.

### Deterrents

TODO.

### Comparison criteria

For the rest of the document, we assume the final solution to be a combination
of some 3rd party solution and some in-house integration software.

- Ease of integration with the Ory hydra's login flow.

- Additional authentication factors that may be supported.

- Quantitative software security
  - Estimated cumulative number of vulnerabilities (according to @Okamura2013Apr)
    (incl transitive dependencies).
  - Quantitative software security functions (according to @Okamura2013Apr)
    (incl transitive dependencies).

- Strategy against security incidents / Security policy.
  - Whether a model validation of the software exists (yes, no).
  - Whether 3rd party audits exist.
  - Evaluations and reviews about the software by security experts.

- Ecosystem
  - Relevance of community contributions.
  - Strength and feedback of user forums.
  - Strength and feedback of developer forums.
  - Key players/stakeholders.

- Integration effort
  - Estimated complexity of what should be implemented to integrate the
    software.
  - Extensibility and future-proofing, limitations.
  - Component standardisation and required level of commitment to the solution.

# References

<!-- Completed by pandoc through the citeproc extension -->
