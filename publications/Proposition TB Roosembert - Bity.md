---
title: BA thesis proposal for Roosembert Palacios at Bity SA
date: 24 Nov 2021
author:
  - Roosembert Palacios
  - Florian Vessaz
---

## Title

Design and implementation of a user authentication system.

## Objective

Enable users to manage their credentials and use them in the existing processes
in place at Bity.

The existing OAuth 2 system in place at Bity allows for a "identification
application" component in charge of determining who the user is.
The existing "identification application" only supports identification with a
username and password.
The idea of the project is to replace the existing "identification application"
in order to support additional sets of credentials and additional authentication
factors.

It is also needed to support the common user needs: setting up credentials,
replacing credentials and recovering an account after losing some credentials.

The objectives of the project are thus:

- Determine sets of credentials that are acceptable for identifying users
  (examples: {username + password}, {username + password + TOTP},
  {username + WebAuthn}, etc.).
- Define processes allowing users to set up their credentials.
- Define processes allowing to replace their credentials.
- Define processes allowing users to recover their accounts after having lost
  their credentials.
- Implement the web application(s) to use as the "identification application"
  and to support the aforementioned processes.

## Phases

- Research of suitable technologies, sets of credentials and processes.
- Design of the processes.
- Implementation of the system without user account recovery and with only two
  sets of possible credentials.
- Implementation of the account recovery process.
- Implementation of the other sets of credentials.

## Deliverables

Must:

- Design document describing:
  - What is acceptable as user credentials.
  - The processes for setup, replacement and usage of user credentials.
  - The processes for recovering an account.

- Web application to use as identification app in the existing modular OAuth2
  system.

- Web application supporting the execution of the account recovery processes.

Wish:

- Adding support for additional sets of credentials.

## Skills

Must:

- Software engineering principles, understanding of security principles, web
  technologies (HTML, CSS, etc.), ability to research and understand new web
  technologies (WebAuthn).

Wish:

- Knowledge of one of the programming languages already used at Bity (Rust,
  TypeScript, Python, Haskell).

## References

- <https://w3c.github.io/webauthn/>
- <https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API>
- <https://datatracker.ietf.org/doc/html/rfc6238>

## Working

- Bity office in Neuchâtel with possibility to work partially remotely.
