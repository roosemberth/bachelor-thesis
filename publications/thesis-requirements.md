---
toc: yes
date: 7 Apr 2022
title: Bachelor thesis requirements
subtitle: Design and implementation of a user authentication system
lang: en
author: Roosembert Palacios
---

# Context

The goal of this bachelor thesis is to provide means for Bity users to manage
their credentials and tokens and use them in the existing processes in place at
Bity.
The deliverables for Bity include a login application Bity clients may use to
identify themselves and a self-service application for managing their
credentials, including additional authentication factors (tokens).
Henceforth, we use the term _login applications_ to refer to both the login
application and the credentials and token management application.

Bity is a Swiss gateway to Cryptocurrencies.
It's mission is to build a universal cryptocurrency gateway that provides secure
and reliable products and services.

Currently, Bity users log in using their username and password.
Bity would like to add additional authentication factors to the existing
authentication scheme.
The objectives of this thesis are to design and prototype a user authentication
system supporting multiple authentication factors (tokens) as defined by NIST's
digital identity guidelines on (@nist-dig):

> The three types of authentication factors are something you know, something
> you have, and something you are. Every authenticator has one or more
> authentication factors

The provided user authentication system should integrate in both the existing
OAuth 2.0 authorization system and the software ecosystem already in place at
Bity.
The solution shall be stored on Bity's software forge with continuous
integration for building, testing and packaging using Bity's well-established
procedures and systems.

# Goals

- Clarify the security implications of additional security factors; and how to
  use them to strengthen the security properties of the system.

- Formally state Bity requirements for the new multi-factors and login
  applications.

- Highlight the best solution to answer Bity's requirements among a custom
  development or some integration of an OAuth-based account and authentication
  system.

- Design a security policy for authenticating a user, replacing user credentials
  and recovering an account after having lost such credentials.

- Get acquainted with current security standards and best practices.
  Understand threat analysis and deliver a threat model with the purpose of
  understanding the system limitations.

- Provide a prototype of the login applications for Bity.

# Phases

## Exploratory research

- Explorative research of existing open source technologies and standards
  facilitating the implementation of the login applications.

- Understanding the existing processes at Bity and the software ecosystem
  where the login applications are to be integrated and deployed.

- Definition of suitable comparison criteria for evaluating an implementation
  strategy of the login applications.

- Definition of the thesis requirements.

## Vision of the solution

- Study security policies applied by standards and open-source solution and
  define a suitable security policy for self-service credential and token
  management:
  Setting credentials and additional security factors (tokens), replacing lost
  credentials or tokens, recovering an account in the case the self-service
  cannot verify the user's identity.

- Analysis of software facilitating the implementation of the login applications
  in order to construct an implementation strategy.

- Initial research report including memoirs of discoveries and accomplished
  learning objectives along with a synthesis of both the exploratory research
  and this phase.

## Design of the solution

- System-level modeling.

- Definition of a [_trusted computing base_ (TCB)][tcb] and a threat model.

- Agree on an implementation strategy for the login applications with Bity.

[tcb]: https://en.wikipedia.org/wiki/Trusted_computing_base

## Prototype iteration 1

- Initial implementation of the login applications.

## Prototype iteration 2

- Deployment of the prototype solution in Bity's staging environment.

- Gather feedback from staging deployment.

## Packaging and delivery

- Add testing, packaging and continuous integration for the login applications.

- Write user and administrator guides of the solution.

- Write configuration, deployment and operations guide.

- Write development documentation.

## Prototype iteration 3 (optional)

This phase will address low-priority features, as defined in the concept and
design such as support for additional security factors (tokens) and deployment
automation.

# Deliverables

## Academic deliverables

- A research document containing:
  - An analysis of the existing system.
  - References to related standards and how they apply.
  - High-level requirements for the new system.
  - A brief memoir relating discoveries and accomplished learning objectives.
  - A comparison criteria for evaluating software facilitating the
    implementation of both the login applications.
  - A comparison of existing open-source solutions.

- A concept and design document containing:
  - A model of the system the solution integrates into.
  - A description of the architecture of the solution.
  - A description of the [_trusted computing base_][tcb].
  - A threat model and a threat analysis.
  - A description of the security policy based on the presented threat model.
    This should specify how we identify an authenticate the user and describe
    procedures for replacing a user credentials and recovering their account
    after having lost such credentials.
  - A list of software components relevant to the system security and
    recommendations for staying up-to-date with security advisories.

## Deliverables for Bity

- A copy of the concept and design document.

- A prototype of the login applications deployed in a staging environment at
  Bity.

- A Git repository including:
  - The prototype of the solution, including any integration code.
  - A development and test environment including documentation on how to set it
    up.
  - Continuous integration for building, testing and packaging in Bity's CI
    infrastructure.
  - Documentation on the configuration, deployment and operation of the
    solution.

# Planning

Start of week | Week number<br/>(academic<br/>calendar) | Number of<br/>work days | Main task
---           | ---  | --- | :---
21.02         | 01   | 2   | Research software solutions providing user authentication
28.02         | 02   | 1   | Review documentation related to existing components at Bity
07.03         | 03   | 1   | Preliminary research on comparison criteria
14.03         | 04   | 0   |
21.03         | 05   | 0   |
28.03         | 06   | 1   | Write documentation about existing components at Bity
04.04         | 07   | 2   | Thesis requirements draft
11.04         | 08   | 2   | Submit thesis requirements
18.04         | -    | 0   |
25.04         | 09   | 2   | Study of security policies by standards and open-source solutions and definition of a suitable policy for the solution.
02.05         | 10   | 2   | Analysis of software facilitating the implementation of both login applications.
09.05         | 11   | 2   | Intermediate report, draft research report.
16.05         | 12   | 2   | System-level design & TCB.
23.05         | 13   | 2   | Submit design documents to Bity for approval of the implementation strategy.
30.05         | 14   | 2   | Prototype iteration 1
06.06         | 15   | 2   | Prototype iteration 1
13.06         | 16   | 2   | Prototype iteration 1
20.06         | (17) | 5   | Prototype iteration 2
27.06         | (18) | 5   | Prototype iteration 2
04.07         | (19) | 5   | Draft thesis report.
11.07         | (20) | 5   | Packaging and delivery.
18.07         | (21) | 5   | Prototype iteration 3.
25.07         | (22) | 5   | Finalize and Submit thesis report

# References

<!-- Completed by citeproc filter -->
