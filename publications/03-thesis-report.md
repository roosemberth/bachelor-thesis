---
date: auto
title: Final project report
subtitle: Design and implementation of a user authentication system
lang: en
toc: yes
author: Roosembert Palacios
---

# Background

The subject of this bachelor project was proposed by Bity, a digital platform
providing financial exchange services to their customers.
The purpose of this bachelor project is to provide Bity with a **practical
prototype** to learn about the challenges and solutions available to implement
**multi-factor authentication** for its customers using credentials and security
tokens (as defined in the subsequent glossary).

## Delimitation

The following subjects, albeit related, are excluded from the present work.
In some cases where some knowledge is necessary to understand an argument
or decision, the reader is given a limited explanation of the subject.

- The evaluation of the security properties of the mechanisms used and proposed
  by the standards used in the present work.
- The evaluation of the security of the implementations of 3rd party software
  used in the present work.
- Federated identities and their use in user authentication.
- The use of external providers to authenticate a user.
- Bity's user account authorization strategy.
- The design and review of a security policy for Bity users and their assets.
- Advanced authorization and access control models that may be used in
  conjunction with the authentication system to improve the security properties
  of Bity's platform.
- The policy of what user authentication (and related) flows available to a
  user at a given time.
- Changes in Bity's existing solution that would better integrate the present
  work.

It should be noted that the author has worked for Bity for the past 4 years
having actively participated on some of the design choices pertaining
Bity's platform.
However, some details have strategically been left out of the present work in
the interest of making it accessible and reproducible for people outside Bity.

## Glossary

[See document: [Glossary](./03.1-glossary.md)]{doc-embed=03.1-glossary.xml}

## Document structure

Sections:

- [sec:formal-reqs]{ref=sec} describes the project **formal requirements** that
  were agreed upon with Bity.
- [sec:sota]{ref=sec} introduces the user authentication problem and
  describes classical solutions and their limitations; it continues by
  describing WebAuthn and TOTP codes and finally provides a justification
  on why these are adequate for the use-case of Bity.
- [sec:description-existing]{ref=sec} contains a description of the existing
  solution in place at Bity for us to learn about the technical opportunities
  and constraints in the perspective of later integration.
- [sec:envisioned-solution]{ref=sec} contains a description of the engineered
  **solution**, containing a specification of **flows to user authentication and
  self-service lost token management**.
  It also broadly describes possible implementations of these specifications.
- [sec:prototype]{ref=sec} gives a practical specification and a
  **prototype** implementation based on the solutions in Section 5.
  This section also describes the **limitations and lessons** learned during the
  development of the prototype, finally providing insights that should be
  addressed before a production deployment.
- [sec:conclusion]{ref=sec} reviews remarkable events during the course of
  the project, providing further insight and draws final recommendations
  based on the project experience.

Annexes:

- [annex:aaa-overview]{ref=annex} contains a description of the
  authentication and authorization strategy in place at Bity.
- [annex:sweng-at-bity]{ref=annex} contains a description of software
  engineering strategies and practices at Bity.
- [annex:threat-analysis]{ref=annex} contains a **threat analysis** of the
  implemented prototype.

# Formal requirements {#sec:formal-reqs}

The solution should provide Bity users with means to manage their credentials
and tokens; Specifically, it should provide strategies (flows) for the
following use-cases:

- User identification.

- Token registration & life-cycle management.

The solution should allow users to configure multi-factor authentication (MFA),
be based on open-source software and adapt to the existing authentication and
authorization strategy in place at Bity (based on OAuth 2.0).
Moreover, it should also integrate in the existing software engineering strategy
at Bity.

The approach for authentication and authorization at Bity is based on OAuth 2.0.
The existing OAuth 2.0 system in place at Bity allows for a "identification
application" component in charge of determining who the user is.
[ORY Hydra][] is used as OAuth 2.0 authorization server.
The interactions between ORY Hydra and the "identification application" in order
to log a user in are very well defined by [Hydra's login flow][hydra-login].
The **solution should implement Hydra's login flow** (depicted in
[fig:hydra-login-flow]{ref=fig}).

[Ory Hydra]: https://www.ory.sh/docs/hydra
[hydra-login]: https://www.ory.sh/docs/hydra/concepts/login

```mermaid
sequenceDiagram
    participant C as OAuth 2.0 Client
    participant Hydra as ORY Hydra
    participant IA as Identification application

    C->>Hydra: Initiates OAuth2 Authorization Code or Implicit Flow
    Hydra-->>Hydra: No end user session available (not authenticated)
    Hydra->>IA: Redirects end user with login challenge
    IA-->Hydra: Fetches login info
    IA-->>IA: Authenticates user with credentials
    IA-->Hydra: Transmits login info and receives redirect url with login verifier
    IA->>Hydra: Redirects end user to redirect url with login verifier
    Hydra-->>Hydra: Handle user consent and grant
    Hydra->>C: Transmits authorization code/token
```

[Hydra login flow]{.figcaption #fig:hydra-login-flow}

Software components at Bity are stored in Bity's software forge (GitLab).
These components are installed via Debian packages, which are generated by
the project's continuous integration (CI).
Packages generated from releases (tags) are then published to either the
staging or production archive by the project maintainer by manually triggering
the adequate job in the CI.
When a new release is published, it is deployed by the corresponding Ansible
playbook through AWX.
The solution should be compatible with this deployment strategy.

A more detailed description of the authentication and authorization
strategy can be found in [annex:aaa-overview]{ref=annex} while a quick
overview of the software engineering strategies and practices at Bity
in section [annex:sweng-at-bity]{ref=annex}.


## Desirable features

To provide SFA, the prototype is required to at least support traditional
username and password.
To simplify integration efforts, the prototype should be self-contained
and manage the users database locally.
The prototype may also provide users with an option to perform SFA using
a WebAuthn credential (colloquially known as **_passwordless authentication_**).

To provide MFA, the prototype should provide at least support for **TOTP-based**
MFA.
WebAuthn MFA is also desirable.

# State of the art {#sec:sota}

[See document: [State of the art](./05.3-state-of-the-art.md)]{doc-embed=05.3-state-of-the-art.xml}

# Description of the existing solution {#sec:description-existing}

[See document: [Existing solution](./05.2-description-existing.md)]{doc-embed=05.2-description-existing.xml}

# Vision of the solution {#sec:envisioned-solution}

[See document: [Solution design](./05.1-solution-design.md)]{doc-embed=05.1-solution-design.xml}

# Prototype {#sec:prototype}

[See document: [Design of a prototype using Keycloak](./05.4-prototype.md)]{doc-embed=05.4-prototype.xml}

# Conclusion {#sec:conclusion}

First, a **formal specification** for the project **was defined** taking into
account the existing systems at Bity.
Subsequently, a review of the state of the art describing **commonly used
authentication factors** was introduced and further developed into a more formal
definition of an authentication factor, followed by a review of **widely-used
standards for user authentication** and open source products implementing these
standards along with a comparison criteria for the evaluation of possible
solutions based on these products.

The standards review revealed many other interesting authentication factors,
but they were quickly excluded because of usability concerns for Bity users.
Even so, the author did find quite surprising that although **smartcard-based
widespread secret-keeping** solutions (such as **PGP**) **have been available
since the 90's**, **Web technologies** seem to **lag behind** by a very long
time (for instance, the first draft for WebAuthn was published in 2016 and
their adoptions is not complete yet even on major platforms).
Moreover, it certainly feels frustrating to see clever **state-of-the-art
solutions** (such as TLS token binding, RFC8471) widely used in proprietary
solutions but **remain unimplemented in** widespread platforms for **the Web**
(such as Web browsers).

Secondly, a set of user authentication strategies addressing the aforementioned
requirements was proposed along with detailed flows for its implementation
leading to the exposition of possible solutions implementing these flows, one
of which is selected for the prototype.

Designing the requirements for a solution based on the problem at hand and
technologies widely available was a very interesting exercise to take a step
back and reconsider the use-cases we're used to on our daily use of the Web.
Not only did this allow the author to better identify issues related to the
prototype implementation;
but also to realize that nowdays, **it is no longer necessary to use**
(and in some cases support) **password-based authentication**.

Finally, a **detailed account** of the **prototype** design and implementation
was presented, along with a threat analysis and some observations leading
to a list of practical considerations to be taken into account by a proper
implementation.

The use of existing solutions to construct the prototype was key in providing
the flexibility of testing many configurations, which ultimately allowed
presenting **a threat analysis** for the implemented solution.
On the other hand, the author was **disappointed at the UX** of the implemented
solution;
when reflecting over why Keycloak implements such interface it became apparent
that it's **a consequence of the modularity and flexibility of security
policies**.
This begs the question whether all of that **flexibility and modularity is
necessary?**
Given that the flow policies are unlikely to change frequent enough.
Either way, this flexibility was a key part of the speed developing and
assessing the security of the prototype.

# References {.backmatter}

<!--
  References are automatically filled by the document toolchain.
  Data in this section will be discarded.
-->

# Authentication and authorization strategy in place at Bity {.annex #annex:aaa-overview}

[See document: [AAA strategy in place at Bity](./03.1-authentication-authorization-strategy-at-Bity.md)]{doc-embed=03.1-authentication-authorization-strategy-at-Bity.xml}

# Software engineering strategies and practices at Bity {.annex #annex:sweng-at-bity}

[See document: [Software engineering strategy and practices at Bity](./03.2-software-engineering-strategies-at-Bity.md)]{doc-embed=03.2-software-engineering-strategies-at-Bity.xml}

# Threat analysis {.annex #annex:threat-analysis}

[See document: [Threat analysis](./04.1-threat-analysis.md)]{doc-embed=04.1-threat-analysis.xml}
