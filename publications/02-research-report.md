---
date: 9 May 2022
title: Research report
subtitle: Design and implementation of a user authentication system
lang: en
author: Roosembert Palacios
---

# Analysis of the existing system

# State of the art

# System-level requirements

# Discoveries and accomplished learning objectives

# Comparison criteria for evaluation of a solution

# Comparison of existing open-source solutions
