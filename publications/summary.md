# Abstract

The purpose of this bachelor project is to provide Bity with a practical
prototype to learn about the challenges and open source solutions available
to implement multi-factor authentication for its customers using credentials
and security tokens.

The project explores commonly used authentication factors, standards for user
authentication and open source projects implementing these standards.
Then, a set of user authentication strategies addressing Bity requirements
is proposed along with detailed flows for user authentication, and lost tokens,
leading to the exposition of possible solutions implementing these flows, one
of which is selected for the prototype.

Finally, a detailed account of the prototype is presented, along with a threat
analysis based on the STRIDE framework along with practical considerations for
a production implementation.

# Résumé

Ce projet a pour but de fournir à Bity un prototype qui permet d'étudier et
comprendre les enjeux pratiques de la mise en place d'un système
d'authentification basé sur l'open source qui fourni des options
d'authentification multi-facteur à ses utilisateurs.

Ce projet explore les facteurs d'authentification les plus utilisés, les
standards à disposition ainsi que les solutions open source implémentant ces
standards.
Ensuite, un ensemble de stratégies autour de l'authentification des utilisateur
sont proposées avec des procédures détaillées pour l'authentification d'un
utilisateur et la gestion en cas de facteurs de sécurité perdus ;
menant à l'exploration des solutions basées sur l'open source qui pourraient
implémenter ces processus dont une est retenue pour le prototype.

Le projet débauche sur un rapport détaillé du prototype avec une analyse de
menaces sur la solution implémentée sur la base de la méthodologie STRIDE et
finalement une discussion sur les considérations pratiques à prendre en
considération lors de la mise en place d'un tel système dans un environnent de
production.
