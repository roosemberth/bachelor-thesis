# Reports and organisation of Roos' bachelor thesis

The repository has the following objectives:

- Hold the thesis reports.
- Coordinate Bity-related tasks and discussions through issues.
- Hold documentation, schemas and logs that do not fit in any other concrete project.
